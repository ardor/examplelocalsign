import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

class ExampleLocalSign {
    private static final HashMap<String, String> applicationResult = new HashMap<>();

    private static ArdorApi ardorApi;
    private static NxtCryptography nxtCryptography;

    private static class Configuration {
        private boolean broadcast = false;
    }

    private static final Configuration configuration = new Configuration();

    public static void main(String[] args) throws Exception {

        if (args.length > 1) {
            applicationResult.put("errorDescription", "usage :\nconfiguration.json");
        } else {

            String configurationFileName = "configuration.json";

            if(args.length == 1) {
                configurationFileName = args[0];
            }

            configurationRead(configurationFileName);

            HashMap<String, String> parameters = new HashMap<>();

            parameters.put("chain", "2");
            parameters.put("recipient", "ARDOR-NZKH-MZRE-2CTT-98NPZ");
            parameters.put("publicKey", nxtCryptography.getPublicKeyString());
            parameters.put("amountNQT", "1");
            parameters.put("feeNQT", "1000000"); // Omit for auto fee. Attention to chain decimals.

            boolean prunableAttachment = true;

            if(prunableAttachment) {
                parameters.put("message", "message");
                parameters.put("messageIsPrunable", "true");
            }

            parameters.put("requestType", "sendMoney");

            JSONObject jsonResponse = ardorApi.jsonObjectHttpApi(true, parameters);

            applicationResult.put("transactionResponse", jsonResponse.toJSONString());

            if(jsonResponse.containsKey("unsignedTransactionBytes")) {
                String unsignedBytesString = (String) jsonResponse.get("unsignedTransactionBytes");
                byte[] unsignedBytes = NxtCryptography.bytesFromHexString(unsignedBytesString);
                ArdorTransaction.signUnsignedTransactionBytes(unsignedBytes, nxtCryptography.getPrivateKey());
                String signedBytesString = NxtCryptography.hexStringFromBytes(unsignedBytes);
                applicationResult.put("signedBytes", signedBytesString);

                if(configuration.broadcast) {

                    parameters = new HashMap<>();

                    if(prunableAttachment) {
                        JSONObject transactionJSON = (JSONObject) jsonResponse.get("transactionJSON");
                        parameters.put("prunableAttachmentJSON", ((JSONObject) transactionJSON.get("attachment")).toJSONString());
                    }

                    parameters.put("transactionBytes", signedBytesString);
                    parameters.put("requestType", "broadcastTransaction");
                    jsonResponse = ardorApi.jsonObjectHttpApi(true, parameters);
                    applicationResult.put("broadcastResponse", jsonResponse.toJSONString());
                } else {
                    applicationResult.put("broadcast", "false");

                }

            } else {
                applicationResult.put("errorDescription", "invalid request");
            }
        }

        JSONObject applicationResultJson = new JSONObject();

        for(String key : applicationResult.keySet()) {
            applicationResultJson.put(key, applicationResult.get(key));
        }

        System.out.println(applicationResultJson.toJSONString());

        System.exit(0);
    }

    private static void configurationRead(String filePath) throws IOException, ParseException {
        File file;

        try {
            file = new File(filePath);
        } catch (Exception e) {
            applicationResult.put("errorDescription", "could not open file : " + filePath);
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            applicationResult.put("errorDescription", "empty configuration : " + filePath);
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfiguration = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));

        if(jsonConfiguration.containsKey("privateKey")) {
            nxtCryptography = new NxtCryptography((String) jsonConfiguration.get("privateKey"));
        } else {
            applicationResult.put("errorDescription", "missing privateKey : " + filePath);
            return;
        }

        configuration.broadcast = JsonSimpleFunctions.getBoolean(jsonConfiguration,"broadcast", configuration.broadcast);

        ardorApi = new ArdorApi((String) ((JSONObject)jsonConfiguration.get("ardorHost")).get("protocol"), (String) ((JSONObject)jsonConfiguration.get("ardorHost")).get("address"), (String) ((JSONObject)jsonConfiguration.get("ardorHost")).get("port"));
    }
}
